<?php

namespace Drupal\youtube_channel;

use Drupal\Core\Queue\QueueFactory;
use Drupal\taxonomy\Entity\Term;


class YouTubeChannelBatch {
  public static function importPlaylist($items, &$context){
    $message = 'Importation des entreprises';

    //\Drupal::logger('my_module')->notice(print_r($items, 1));

    $results = array();
    foreach($items as $item){

      //\Drupal::logger('my_module')->notice('<pre>'.print_r($item, 1).'</pre>');

      $id = $item['id'];
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('vid', "ycp");
      $query->condition('field_ycp_id', "$id");
      $results = $query->execute();

      if(!empty($results)){
        $tid = key($results);

        $term = Term::load($tid);
        $term->name->setValue($item['snippet']['title']);

        //$term->create->setValue(strtotime($item['snippet']['publishedAt']));
        $term->Save();



        //\Drupal::logger('my_module')->notice('<pre>'.print_r($term, 1).'</pre>');
      }
      else{
        $term = Term::create(array(
          'parent' => array(),
          'name' => $item['snippet']['title'],
          'vid' => 'ycp',
          //'created' => strtotime($item['snippet']['publishedAt']),
          'field_ycp_id' => [$id]
        ))->save();
      }
      //\Drupal::logger('my_module')->notice('<pre>'.print_r($tids, 1).'</pre>');



      /*$id = $item['id'];
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('vid', "ycp");
      $query->condition('name', "My tag");
      $tids = $query->execute();*/


      /*$query = \Drupal::entityQuery('node')
        ->condition('type', 'enterprise')
        ->condition('field_enterprise_id', $item->accountid);
      $nids = $query->execute();

      if(!empty($nids)) {
        $nid = key($nids);
        $enterprise = Node::load($nid);
        $enterprise->set('title', Html::escape($item->name));
        $enterprise->set('field_enterprise_address', Html::escape($item->new_adresse));
        $enterprise->save();
      }
      else{
        $enterprise = Node::create(['type' => 'enterprise']);
        $enterprise->set('title', Html::escape($item->name));
        $enterprise->set('field_enterprise_id', $item->accountid);
        $enterprise->set('field_enterprise_address', Html::escape($item->new_adresse));
        $enterprise->save();
      }*/
      $results[] = $item;
    }
    $context['message'] = $message;
    $context['results'] = $results;

  }
  public static function FinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One node processed.', '@count nodes processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }
}