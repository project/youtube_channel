<?php

namespace Drupal\youtube_channel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\youtube_channel\YouTubeChannel;



/**
 * Configure youtube_channel settings for this site.
 */
class YoutubeChannelSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_channel_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'youtube_channel.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('youtube_channel.settings');



    /*****************************/
    /*$entityManager = \Drupal::service('entity.manager');
    $fields = $entityManager->getFieldDefinitions('node', 'ycv');
    kint($fields);*/


    /*$entityFieldManager = \Drupal::service('entity_field.manager');

    $ycv_fields = $entityFieldManager->getFieldDefinitions('node', 'ycv');

    if (isset($ycv_fields['field_ycv_id'])) {
      $ycv_fields['field_ycv_id']->delete();
    }*/


    $form['youtube_conf'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('YouTube channel configurations'),
      '#collapsible' => FALSE,
    ];

    $form['youtube_conf']['api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('YouTube API Key'),
      '#size' => 40,
      '#default_value' => !empty($config->get('api_key')) ? $config->get('api_key') : '',
      '#required' => TRUE,
      '#description' => $this->t('Your YouTube Google API key from your developer'),
    );

    $form['youtube_conf']['channel_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Youtube Channel ID'),
      '#size' => 40,
      '#default_value' => !empty($config->get('channel_id')) ? $config->get('channel_id') : '',
      '#required' => TRUE,
      '#description' => $this->t('The youtube channel ID you want to get the videos.'),
    );

    $form['youtube_import'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Video Import'),
      '#collapsible' => FALSE,
    ];

    $form['youtube_import']['cron'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Import video using cron'),
      '#default_value' => !empty($config->get('cron')) ? $config->get('cron') : NULL,
    );

    $form['actions']['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration and import videos'),
      '#submit' => [[$this, 'import_video']],
      '#weight' => 10
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (substr($form_state->getValue('channel_id'), 0, 2) !== "UC") {
      $form_state->setErrorByName('channel_id', $this->t('Not a valid Youtube Channel ID'));
    }
  }

  public function import_video(array &$form, FormStateInterface &$form_state) {
    $api_key = $form_state->getValue('api_key');
    $channel_id = $form_state->getValue('channel_id');
    $this->config('youtube_channel.settings')
    ->set('api_key', $api_key)
    ->set('channel_id', $channel_id)
    ->set('cron', $form_state->getValue('cron'))
    ->save();

    $operations = [];

    $query = [
      'key' => $api_key,
      'part' => 'snippet,id',
      'order' => 'date',
      'channelId' => $channel_id,
      'maxResults' => 50,
    ];
    $channel_data = _youtube_channel_query($query, 'playlists');


    //\Drupal::logger('YouTube Channel')->notice('<pre>'.print_r($channel_data, 1).'</pre>');


    if(isset($channel_data['items']) && count($channel_data['items']) > 0) {
      $operations[] = array('\Drupal\youtube_channel\YouTubeChannelBatch::importPlaylist', array($channel_data['items']));
    }
    $batch = array(
      'title' => t('Import videos'),
      'operations' => $operations,
      'finished' => '\Drupal\grenoble_enterprise\YouTubeChannelBatch::FinishedCallback',
    );
    batch_set($batch);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('youtube_channel.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('channel_id', $form_state->getValue('channel_id'))
      ->set('cron', $form_state->getValue('cron'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}