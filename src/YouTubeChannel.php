<?php
/**
 * Created by PhpStorm.
 * User: Kumaren
 * Date: 9/6/2017
 * Time: 1:26 PM
 */

namespace Drupal\youtube_channel;


class YouTubeChannel {

  public function __construct() {

    drupal_set_message(t('Hello'));
  }

  public function get(){

  }

  /**
   * Returns a list of standard YouTube video sizes.
   */
  public function sizeOptions() {
    return array(
      '450x315' => '450px by 315px',
      '480x360' => '480px by 360px',
      '640x480' => '640px by 480px',
      '960x720' => '960px by 720px',
      'responsive' => 'responsive (full-width of container)',
      'custom' => 'custom',
    );
  }

} 