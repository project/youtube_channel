<?php

namespace Drupal\youtube_channel\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_example_rgb' field type.
 *
 * @FieldType(
 *   id = "youtube_channel_field_video_type",
 *   label = @Translation("Video"),
 *   module = "youtube_channel",
 *   description = @Translation("YouTube Channel YouTube field."),
 *   category = @Translation("YouTube Channel"),
 *   default_widget = "youtube_channel_field_video_widget",
 *   default_formatter = "youtube_channel_field_video_formatter"
 * )
 */
class VideoFieldType extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'YouTube Video ID.',
          'type' => 'varchar',
          'length' => 1024,
          'not null' => FALSE,
        ],
      ],
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Hex value'));

    return $properties;
  }

}
