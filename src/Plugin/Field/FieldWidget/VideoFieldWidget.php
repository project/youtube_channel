<?php

/**
 * @file
 * Contains \Drupal\youtube\Plugin\Field\FieldWidget\YouTubeDefaultWidget.
 */

namespace Drupal\youtube_channel\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'youtube_default' widget.
 *
 * @FieldWidget(
 *   id = "youtube_channel_field_video_widget",
 *   module = "youtube_channel",
 *   label = @Translation("YouTube video ID"),
 *   field_types = {
 *     "youtube_channel_field_video_type"
 *   },
 * )
 */
class VideoFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    return $element;
  }

  /**
   * Validate video URL.
   */
  public function validateInput(&$element, FormStateInterface &$form_state, $form) {

  }

}
